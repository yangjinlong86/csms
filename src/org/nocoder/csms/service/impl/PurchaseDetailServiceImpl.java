package org.nocoder.csms.service.impl;

import java.util.List;

import org.nocoder.csms.dao.PurchaseDetailDao;
import org.nocoder.csms.entity.PurchaseDetail;
import org.nocoder.csms.service.PurchaseDetailService;

public class PurchaseDetailServiceImpl implements PurchaseDetailService {
	
	private PurchaseDetailDao dao = new PurchaseDetailDao();

	@Override
	public List<PurchaseDetail> getPurchaseDetailByPId(String pid) {
		return dao.selectByPurchaseId(pid);
	}

	@Override
	public int savePurchaseDetail(PurchaseDetail purDetail) {
		return dao.insertPurchaseDetail(purDetail);
	}

	@Override
	public int updatePurchaseDetailById(PurchaseDetail purDetail) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deletePurchaseDetailById(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

}
