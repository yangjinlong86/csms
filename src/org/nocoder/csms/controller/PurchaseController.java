package org.nocoder.csms.controller;

import org.nocoder.csms.entity.Purchase;
import org.nocoder.csms.entity.PurchaseDetail;
import org.nocoder.csms.service.PurchaseDetailService;
import org.nocoder.csms.service.PurchaseService;
import org.nocoder.csms.service.impl.PurchaseDetailServiceImpl;
import org.nocoder.csms.service.impl.PurchaseServiceImpl;
/**
 * 进货记录controller
 * @author yangjinlong
 *
 */
public class PurchaseController {
	private PurchaseService purchaseService = new PurchaseServiceImpl();
	private PurchaseDetailService purchaseDetailService = new PurchaseDetailServiceImpl();
	
	public int savePurchase(Purchase pur){
		String purId = purchaseService.savePurchase(pur);
		if(purId != null && !"".equals(purId)){
			for(PurchaseDetail detail : pur.getPurchaseDetailList()){
				detail.setPurchaseId(pur.getId());
				purchaseDetailService.savePurchaseDetail(detail);
				System.out.println("进货明细保存成功！");
			}
		}
		return 0;
	}
	public int updatePurchaseById(Purchase pur){
		return purchaseService.updatePurchaseById(pur);
	}
	public int deletePurchaseById(String id){
		return purchaseService.deletePurchaseById(id);
	}
	public Purchase getPurchaseById(String id){
		return purchaseService.getPurchaseById(id);
	}
}
