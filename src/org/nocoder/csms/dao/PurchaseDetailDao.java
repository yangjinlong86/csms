package org.nocoder.csms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.nocoder.csms.entity.PurchaseDetail;
import org.nocoder.csms.util.DBUtil;

public class PurchaseDetailDao {
	// select by purchase id
	public List<PurchaseDetail> selectByPurchaseId(String pid){
		Connection conn = DBUtil.getConnection();
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		List<PurchaseDetail> list = null;
		sql.append("SELECT ID, PURCHASE_ID, PURCHASE_DATE,TYPE, QUANTITY, ")
			.append("PURCHASE_PRICE, SOURCE_OF_GOODS,TOTAL_PRICE ")
			.append("FROM PURCHASE_DETAIL WHERE PURCHASE_ID = ?");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, pid);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				PurchaseDetail pd = new PurchaseDetail();
				pd.setId(rs.getString("ID"));
				pd.setPurchaseDate(rs.getDate("PURCHASE_DATE"));
				pd.setPurchaseId(rs.getString("PURCHASE_ID"));
				pd.setPurchasePrice(rs.getDouble("PURCHASE_PRICE"));
				pd.setQuantity(rs.getInt("QUANTITY"));
				pd.setSourceOfGoods(rs.getString("SOURCE_OF_GOODS"));
				pd.setTotalPrice(rs.getDouble("TOTAL_PRICE"));
				pd.setType(rs.getString("TYPE"));
				list = new ArrayList<PurchaseDetail>();
				list.add(pd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closePreparedStatement(ps);
			DBUtil.closeConnection(conn);
		}
		return list;
	}
	
	// save purchase detail 
	public int insertPurchaseDetail(PurchaseDetail pd){
		Connection conn = DBUtil.getConnection();
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO PURCHASE_DETAIL (")
			.append("ID, PURCHASE_ID, PURCHASE_DATE, TYPE, QUANTITY,") 
			.append("PURCHASE_PRICE, SOURCE_OF_GOODS, TOTAL_PRICE) VALUES (")
			.append("?,?,?,?,?,?,?,?")
			.append(")");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, UUID.randomUUID().toString().replace("-", ""));
			ps.setString(2, pd.getPurchaseId());
			ps.setDate(3, pd.getPurchaseDate());
			ps.setString(4, pd.getType());
			ps.setDouble(5, pd.getQuantity());
			ps.setDouble(6, pd.getPurchasePrice());
			ps.setString(7, pd.getSourceOfGoods());
			ps.setDouble(8, pd.getTotalPrice());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closePreparedStatement(ps);
			DBUtil.closeConnection(conn);
		}
		return 0;
	}
}
