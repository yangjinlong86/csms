package org.nocoder.csms.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.nocoder.csms.controller.ClientUIController;
import org.nocoder.csms.controller.PurchaseController;
import org.nocoder.csms.entity.Purchase;
import org.nocoder.csms.entity.PurchaseDetail;

public class PurchaseFrame extends BaseJFrame {
	private static final long serialVersionUID = -1456299737921732743L;

	public PurchaseFrame() {
		this.setTitle("进货记录");
		this.setContentPane(createContentPane());
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				clientUIController.convertJFrame(clientUIController.getPurchaseFrame(), clientUIController.getPurchaseListFrame());
			}

		});
	}

	private JPanel createContentPane() {
		JPanel pane = new JPanel(new BorderLayout());
		pane.add(BorderLayout.NORTH, createInputPane());
		pane.add(BorderLayout.CENTER, createTablePane());
		pane.add(BorderLayout.SOUTH, createButtonPanel());
		return pane;
	}

	private JPanel createInputPane() {
		JPanel inputPanel = new JPanel(new GridLayout(1, 4));
		
		JPanel batchNamePanel = new JPanel(new BorderLayout(6, 0));
		batchNamePanel.add(BorderLayout.WEST, new JLabel("*批次名称"));
		batchName = new JTextField();
		batchNamePanel.add(BorderLayout.CENTER, batchName);
		
		JPanel totalCostPanel = new JPanel(new BorderLayout(6, 0));
		totalCostPanel.add(BorderLayout.WEST, new JLabel("总成本"));
		totalCost = new JTextField();
		totalCostPanel.add(BorderLayout.CENTER, totalCost);
		
		JPanel expressCostPanel = new JPanel(new BorderLayout(6, 0));
		expressCostPanel.add(BorderLayout.WEST, new JLabel("*运费"));
		expressCost = new JTextField("");
		expressCostPanel.add(BorderLayout.CENTER, expressCost);
		
		JPanel remarkPanel = new JPanel(new BorderLayout(6, 0));
		remarkPanel.add(BorderLayout.WEST, new JLabel("备注"));
		remark = new JTextField();
		remarkPanel.add(BorderLayout.CENTER, remark);
		
		inputPanel.add(batchNamePanel);
		inputPanel.add(totalCostPanel);
		inputPanel.add(expressCostPanel);
		inputPanel.add(remarkPanel);
		return inputPanel;
	}

	private JPanel createTablePane() {
		JPanel panel = new JPanel(new BorderLayout());
		// create table column names
		String[] columnNames = {"进货日期", "服装类别", "进价", "数量"};
		
		// create table data
		Object[][] data = {};
		
		// create a model
		tableModel = new DefaultTableModel(data, columnNames);
		
		// create a table
		table = new JTable(tableModel);
		
		// table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); 
		
		table.setGridColor(Color.GRAY);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setSelectionBackground(Color.LIGHT_GRAY);
		table.setSelectionForeground(Color.BLUE);
		
		panel.add(createToolBarPanel(), BorderLayout.NORTH);
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		return panel;
	}
	
	/**
	 * 表格操作区域
	 * @return toolbarPanel
	 */
	private JPanel createToolBarPanel(){
		JPanel toolbarPanel = new JPanel(new BorderLayout());
		JPanel btnPanel = new JPanel();
		JButton addRowBtn = new JButton("添加");
		addRowBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				tableModel.addRow(new Object[]{new Date(System.currentTimeMillis()), "", "", ""});
			}
		});
		
		JButton deleteRow = new JButton("删除");
		deleteRow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRow() == -1){
					System.out.println("未选中行数据");
					return;
				}
				tableModel.removeRow(table.getSelectedRow());
				
			}
		});
		
		btnPanel.add(addRowBtn);
		btnPanel.add(deleteRow);
		toolbarPanel.add(btnPanel, BorderLayout.EAST);
		return toolbarPanel;
	}

	/**
	 * 保存、取消 按钮区域
	 * 
	 * @return
	 */
	private JPanel createButtonPanel() {
		JPanel panel = new JPanel(new FlowLayout());
		JButton saveBtn = new JButton("保存");
		saveBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String batchNameStr = batchName.getText();
				if(batchNameStr == null || "".equals(batchNameStr)){
					System.out.println("批次名称不能为空");
					return;
				}
				
				String totalCostStr = totalCost.getText();
				if(totalCostStr == null || "".equals(totalCostStr)){
					System.out.println("本次总成本不能为空");
					return;
				}
				
				Purchase pur = new Purchase();
				pur.setBatchName(batchNameStr);
				pur.setTotalCost(Double.valueOf(totalCostStr));
				pur.setExpressCost(Double.valueOf(expressCost.getText()));
				pur.setRemark(remark.getText());
				
				// add purchase detail list to Purchase 
				TableModel tableModel = table.getModel();
				int rowcount = tableModel.getRowCount();
				List<PurchaseDetail> pList = new ArrayList<PurchaseDetail>();
				PurchaseDetail pd = null;
				for(int i=0; i<rowcount; i++){
					pd = new PurchaseDetail();
					pd.setPurchaseDate((Date) tableModel.getValueAt(i, 0));
					pd.setType((String) tableModel.getValueAt(i, 1));
					pd.setPurchasePrice(Double.valueOf((String)tableModel.getValueAt(i, 2)));
					pd.setQuantity(Integer.valueOf((String) tableModel.getValueAt(i, 3)));
					pList.add(pd);
				}
				pur.setPurchaseDetailList(pList);
				purchaseController.savePurchase(pur);
				
				clientUIController.convertJFrame(clientUIController.getPurchaseFrame(), clientUIController.getPurchaseListFrame());
			}
		});
		
		JButton closeBtn = new JButton("关闭");
		closeBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				clientUIController.convertJFrame(clientUIController.getPurchaseFrame(), clientUIController.getPurchaseListFrame());
				
			}
		});
		
		panel.add(saveBtn);
		panel.add(closeBtn);
		getRootPane().setDefaultButton(saveBtn);
		return panel;
	}

	public static void main(String[] args) {
		PurchaseFrame purchaseFrame = new PurchaseFrame();
		purchaseFrame.setVisible(true);
	}
	
	private JTextField batchName = null;
	private JTextField totalCost = null;
	private JTextField expressCost = null;
	private JTextField remark = null;
	private JTable table = null;
	private DefaultTableModel tableModel = null;
	
	private ClientUIController clientUIController;

	public void setClientUIController(ClientUIController controller) {
		this.clientUIController = controller;
	}
	
	private PurchaseController purchaseController;

	public void setPurchaseController(PurchaseController purchaseController) {
		this.purchaseController = purchaseController;
	}
	
	
}
