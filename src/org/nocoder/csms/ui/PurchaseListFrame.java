package org.nocoder.csms.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.nocoder.csms.controller.ClientUIController;

public class PurchaseListFrame extends BaseJFrame {

	private static final long serialVersionUID = -5102434642927891991L;

	public PurchaseListFrame() {
		this.setTitle("进货记录");
		this.setContentPane(createContentPane());
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				clientUIController.convertJFrame(clientUIController.getPurchaseListFrame(), clientUIController.getMainFrame());
			}

		});
	}
	
	private JPanel createContentPane(){
		JPanel panel = new JPanel(new BorderLayout());
		
		String [] columnNames = {"批次名称", "创建日期", "成本"};
		Object[][] data = {};
		DefaultTableModel tableModel = new DefaultTableModel(data, columnNames);
		table = new JTable(tableModel);
		
		JPanel toolbarPane = new JPanel(new BorderLayout());
		
		JPanel addBtnPane = new JPanel();
		JButton addBtn = new JButton("添加");
		addBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clientUIController.convertJFrame(clientUIController.getPurchaseListFrame(), clientUIController.getPurchaseFrame());
				
			}
		});
		addBtnPane.add(addBtn);
		
		JPanel btnPane = new JPanel();
		JButton editBtn = new JButton("修改");
		editBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clientUIController.convertJFrame(clientUIController.getPurchaseListFrame(), clientUIController.getPurchaseFrame());
				
			}
		});
		btnPane.add(editBtn);
		
		JButton removeBtn = new JButton("删除");
		btnPane.add(removeBtn);
		
		toolbarPane.add(addBtnPane, BorderLayout.WEST);
		toolbarPane.add(btnPane, BorderLayout.EAST);
		panel.add(toolbarPane, BorderLayout.NORTH);
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		return panel;
	}
	private JTable table;
	private ClientUIController clientUIController;

	public void setClientUIController(ClientUIController clientUIController) {
		this.clientUIController = clientUIController;
	}
	
}
